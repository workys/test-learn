package com.test.hutool;

import com.test.hutool.bean.MyHttpRequest;
import com.test.hutool.bean.MyHttpResponse;

public class MyHttpClient {
    private static MyHttpClient myHttpClient;

    public static MyHttpClient getInstance() {
        synchronized (MyHttpClient.class) {
            if (myHttpClient == null) {
                myHttpClient = new MyHttpClient();
            }
        }
        return myHttpClient;
    }

    public MyHttpResponse execute(String url){
        MyHttpRequest myHttpRequest = new MyHttpRequest(url);
        return myHttpRequest.execute();
    }

}
