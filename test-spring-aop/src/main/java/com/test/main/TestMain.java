package com.test.main;

import com.test.service.AspectService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMain {

    @Test
    public void test_Main(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        AspectService aservice = context.getBean(AspectService.class);
        System.out.println("\n===============普通调用===============\n");

        aservice.sayHi("lilei");

        System.out.println("\n===============异常调用===============\n");
        aservice.excuteException();

        System.out.println("\n=====================================\n");
    }

}
