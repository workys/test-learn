package com.test.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SimpleAspect {

    private final String POINT_CUT = "execution(* com..service.*.*(..))";

    @Pointcut(POINT_CUT)
    public void pointCut() {

    }

    @Before(value = "pointCut()")
    public void doBefore(JoinPoint joinPoint) {
        System.out.println("@Before:切点方法之前执行.....");
    }

    @After(value = "pointCut()")
    public void doAfter(JoinPoint joinPoint) {
        System.out.println("@After:切点方法之后执行....");
    }

    @AfterReturning(value = "pointCut()", returning = "result")
    public void doAfter(JoinPoint joinPoint, Object result) {
        System.out.println("@AfterReturning: 切点方法返回后执行....");
        System.out.println("返回值：" + result);
    }

    @AfterThrowing(value = "pointCut()",throwing = "exception")
    public void doAfterThrowing(JoinPoint joinPoint,Throwable exception) {
        System.out.println("@afterThrowing:切点方法抛异常执行....");
    }

    @Around(value = "pointCut()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("@Around:切点方法环绕start....");
        Object[] args = pjp.getArgs();
        Object proceed = pjp.proceed(args);
        System.out.println("@Around:切点方法环绕stop....");
        return proceed;
    }

}
