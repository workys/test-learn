package com.test.ys;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.mockito.Mockito.*;

public class MockTest {
    @Test
    public void createMockObject() {
        // 使用 mock 静态方法创建 Mock 对象.
        List mockedList = mock(List.class);
        Assert.assertTrue(mockedList instanceof List);
        //当我们有了一个 Mock 对象后, 我们可以定制它的具体的行为
        //当调用 mockedList.add("one") 时, 返回 true
        //当调用 mockedList.size() 时, 返回 1
        when(mockedList.add("one")).thenReturn(true);
        when(mockedList.add("two")).thenReturn(true);
        when(mockedList.size()).thenReturn(2);
       // when(list.add("one")).thenReturn(true);
        Assert.assertTrue(mockedList.add("one"));
        // 因为我们没有定制 add("two"), 因此返回默认值, 即 false.
        Assert.assertTrue(mockedList.add("two"));
        Assert.assertEquals(2,mockedList.size());

        // mock 方法不仅可以 Mock 接口类, 还可以 Mock 具体的类型.
        ArrayList mockedArrayList = mock(ArrayList.class);
        Assert.assertTrue(mockedArrayList instanceof List);
        Assert.assertTrue(mockedArrayList instanceof ArrayList);

        Map mockedmap = mock(Map.class);


        Iterator i = mock(Iterator.class);
        when(i.next()).thenReturn("Hello,").thenReturn("Mockito!");
        String result = i.next() + " " + i.next() + " " + i.next();
        //assert
        Assert.assertEquals("Hello, Mockito! Mockito!", result);

        int[] mock = mock(int[].class);
        when(mock).thenReturn(new int[]{0,1});
        int[] a =  mock;
        System.out.println(a);

    }

    @Test(expected = NoSuchElementException.class)
    public void testForIOException() throws Exception {
        Iterator i = mock(Iterator.class);
        when(i.next()).thenReturn("Hello,").thenReturn("Mockito!"); // 1
        String result = i.next() + " " + i.next()+ " " + i.next(); // 2
        Assert.assertEquals("Hello, Mockito! Mockito!", result);
        doThrow(new NoSuchElementException()).when(i).next(); // 3
        i.next(); // 4
    }
    @Test
    public void test1(){
        LinkedList mockedList = mock(LinkedList.class);

        //stubbing
        when(mockedList.get(0)).thenReturn("first");
        when(mockedList.get(1)).thenThrow(new RuntimeException());

        //following prints "first"
        System.out.println(mockedList.get(0));

        //following throws runtime exception
        //System.out.println(mockedList.get(1));

        //following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));
    }
}
