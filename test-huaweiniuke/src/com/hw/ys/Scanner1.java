package com.hw.ys;

import java.util.Scanner;

public class Scanner1 {

    public static void main(String[] args) {
        inputStr();
        inputInteger();
        inputIntInLine();
    }

    private static void inputStr() {
        Scanner scanner = new Scanner(System.in);
        String nextLine = scanner.nextLine();
        while (nextLine != null && !nextLine.equals("")) {
            System.out.println(nextLine);
            nextLine = scanner.nextLine();
        }
        System.out.println("end of input string");
    }

    private static void inputIntInLine() {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String[] numstr = str.split(" ");
        int[] nums = new int[numstr.length];
        for (int i = 0; i < numstr.length; i++) {
            nums[i] = Integer.parseInt(numstr[i]);
        }
        for (int num : nums) {
            System.out.println(num);
        }
        System.out.println("end of input int in a line");
    }

    public static void inputInteger() {
        Scanner scanner = new Scanner(System.in);
        String nextline = scanner.nextLine();
        int sum = 0;

        while (nextline != null && !nextline.equals("")){
            sum += Integer.parseInt(nextline);
            System.out.println(sum);
            nextline = scanner.nextLine();
        }
        System.out.println("end of input integer");
    }

}
