package com.hw.ys;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class Main1 {

    public static void getLastLength() {
        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine().trim();
        String[] arr = line.split(" ");
        System.out.println(arr[arr.length-1].length());
    }

    public static void getLastLength1() throws IOException  {
        int times = 0;
        InputStream in = System.in;
        char c = (char) in.read();
        while (c != '\n') {
            if (c == ' ') {
                times = 0;
            } else {
                times += 1;
            }
            c = (char) in.read();
        }
        System.out.println(times);
    }

    public static void main(String[] args) throws IOException {
        getLastLength1();
    }
}
