package com.hw.ys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Main2 {
    public static void getSum() throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String str = bf.readLine();
        String flag = bf.readLine();
        System.out.println(getCount(str,flag));

    }

    private static int getCount(String str, String flag) {
        String strtolow = str.toLowerCase();
        String flagtolow = flag.toLowerCase();
        int count = 0;
        for (int i = 0; i < strtolow.length(); i++) {
            char c = strtolow.charAt(i);
            if (String.valueOf(c).equals(flagtolow)){
                count+=1;
            }
        }
        return count;
    }

    public static void main(String[] args) throws IOException {
        getSum();
    }
}
