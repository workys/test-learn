package com.hw.ys;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class Main3 {

    public static void sort() {
        Scanner sc = new Scanner(System.in);
        //获取个数
        while (sc.hasNext()) {
            int num = sc.nextInt();
            if( num == -1){
                break;
            }
            //创建TreeSet进行去重排序
            TreeSet set = new TreeSet();
            //输入
            for (int i = 0; i < num; i++) {
                set.add(sc.nextInt());
            }

            //输出
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        }
        sc.close();
    }

    public static void sort1(){
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            if(n == -1){
                break;
            }
            int[] intArr = new int[n];
            for (int i = 0; i < n; i++) {
                intArr[i] = scanner.nextInt();
            }
            Arrays.sort(intArr);
            for (int i = 0; i < intArr.length; i++) {
                // 第一个数字或者不等于前一个数字都可以输出
                if (i == 0 || intArr[i] != intArr[i - 1]) {
                    System.out.println(intArr[i]);
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        /*BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String str;
        while ((str = bf.readLine()) != null) {
            boolean[] stu = new boolean[1001];
            StringBuilder sb = new StringBuilder();
            int n = Integer.parseInt(str);
            for (int i = 0; i < n; i++) {
                stu[Integer.parseInt(bf.readLine())] = true;
            }
            for (int i = 0; i < 1001; i++)
                if (stu[i])
                    sb.append(i).append("\n");
            sb.deleteCharAt(sb.length() - 1);
            System.out.println(sb.toString());*/
        sort1();
    }
}


