package com.ys.util;


import java.util.ArrayList;

public class ListNode {
    public int val;
    public ListNode next;

    ListNode(int x) {
        val = x;
        next = null;
    }

   public static int[] ListNodeToArray(ListNode listNode) {
        ArrayList<Integer> al = new ArrayList<Integer>();
        int i = 0;
        while (listNode != null) {
            al.add(i++, listNode.val);
            listNode = listNode.next;
        }
        return al.stream().mapToInt(Integer::intValue).toArray();
    }

    public static ListNode ArrayToListNode(int[] arr) {
        ListNode listNode = null;
        ListNode listNode1 = null;
        for (int i = arr.length - 1; i >= 0; i--) {
            listNode = new ListNode(arr[i]);
            listNode.next = listNode1;
            listNode1 = listNode;

        }
        return listNode;
    }




}
