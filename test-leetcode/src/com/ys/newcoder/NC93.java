package com.ys.newcoder;

import sun.applet.Main;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author yangs
 * @version 1.0
 * @description: 设计LRU缓存结构
 * @date 2021-6-28 20:17
 */
public class NC93 {
    private Map<Integer, Node> map = new HashMap<>();
    private Node head = new Node(-1, -1);
    private Node tail = new Node(-1, -1);
    private int k;

    public int[] LRU(int[][] operators, int k) {
        this.k = k;
        head.next = tail;
        tail.prev = head;
        int len = Math.toIntExact(Arrays.stream(operators).filter(x -> x[0] == 2).count());
        int[] res = new int[len];
        for (int i = 0, j = 0; i < operators.length; i++) {
            if (operators[i][0] == 1) {
                set(operators[i][1], operators[i][2]);
            } else {
                res[j++] = get(operators[i][1]);
            }
        }
        return res;
    }

    private void set(int key, int value) {
        if (map.containsKey(key)) {
            map.get(key).val = value;
        } else {
            if (map.size() == k) {
                int rk = tail.prev.key;
                tail.prev.prev.next = tail;
                tail.prev = tail.prev.prev;
                map.remove(rk);
            }
            Node node = new Node(key, value);
            map.put(key, node);
            moveToHead(node);
        }
    }

    private int get(int key) {
        if (map.containsKey(key)) {
            Node node = map.get(key);
            node.prev.next = node.next;
            node.next.prev = node.prev;
            return node.val;
        }
        return -1;
    }

    private void moveToHead(Node node) {
        node.next = head.next;
        head.next.prev = node;
        node.prev = head;
        head.next = node;

    }

    static class Node {
        int key, val;
        Node prev, next;

        public Node(int key, int val) {
            this.key = key;
            this.val = val;
        }
    }

    public static void main(String[] args) {
        NC93 nc93 = new NC93();
        int[][] arr = {{1,1,1},{1,2,2,},{1,3,4,},{1,2,4},{2,5},{2,3},{2,1},{1,8,2}};
        int[] lru = nc93.LRU(arr, 2);
        System.out.println(Arrays.toString(lru));
    }
}
