package com.ys.newcoder;

import com.ys.util.ListNode;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import static com.ys.util.ListNode.ArrayToListNode;
import static com.ys.util.ListNode.ListNodeToArray;

/**
 * @author yangs
 * @version 1.0
 * @description: 反转链表
 * @date 2021-6-30 19:13
 */
public class NC78 {

    //正规解法
    public ListNode reverSeListByZg(ListNode head) {
        ListNode pre = null;
        ListNode p = null;
        while (head != null) {
            p = head.next;
            head.next = pre;
            pre = head;
            head = p;
        }
        return pre;
    }

    //入栈出栈
    public ListNode reverSeListByStack(ListNode head) {
        Stack<ListNode> stack = new Stack<>();
        while (head != null) {
            stack.push(head);
            head=  head.next;
        }
        if (stack.isEmpty()) {
            return null;
        }
        ListNode node = stack.pop();
        ListNode dummy = node;
        while (!stack.isEmpty()) {
            ListNode tempNode = stack.pop();
            node.next =tempNode;
            node = node.next;
        }
        node.next = null;
        return  dummy;
    }


    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4};
        ListNode listNode = ArrayToListNode(arr);
        NC78 nc = new NC78();
        ListNode listNode1 = nc.reverSeListByZg(listNode);
        int[] ints = ListNodeToArray(listNode1);
        System.out.println(Arrays.toString(ints));
    }


}
