package com.ys.proxy.unity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class User {

    private String name;
    private Integer age;
    private String password;
}
