package com.ys.proxy.service;

import com.ys.proxy.unity.User;

public interface UserService {
    void addUser(User user);
}
