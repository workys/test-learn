package com.ys.proxy.service.impl;

import com.ys.proxy.service.UserService;
import com.ys.proxy.unity.User;

public class UserServiceImpl implements UserService {
    @Override
    public void addUser(User user) {
        System.out.println("jdk...正在注册用户， 用户信息为："+user);
    }
}
