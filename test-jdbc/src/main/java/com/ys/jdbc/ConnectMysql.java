package com.ys.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class ConnectMysql {
    public static void main(String[] args) {
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://172.19.3.41:3306/apsdevdb";
        String user = "aps";
        String password = "apsdev2021@";

        try {
            Class.forName(driver);
            Connection conn = DriverManager.getConnection(url, user, password);
            if (!conn.isClosed()) {
                System.out.println("Succeeded connecting to the Database!");
                Statement statement = conn.createStatement();
                String sql = "select * from aos_user_param";
                ResultSet rs = statement.executeQuery(sql);
                String name;
                while (rs.next()) {

                    name = rs.getString("cparam");
                    System.out.println(name);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

}
