package com.test.jedis;
import com.test.ys.jedis.JedisUtils;
import org.junit.Test;
import redis.clients.jedis.Jedis;

public class TestJedisDemo {


    @Test
    public void run() {
        Jedis jedis = new Jedis("192.168.122.3", 6379);
        jedis.auth("123456");
        jedis.set("sex", "男");

        System.out.println(jedis.get("sex"));
        System.out.println(jedis.get("1"));
    }

    @Test
    public void testRun() {
        Jedis jedis = JedisUtils.getJedis();
        jedis.expire("1", 10);
        System.out.println(jedis.get("1"));

        jedis.close();
    }

}
