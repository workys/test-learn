package com.test.ys.jedis;

import com.sun.org.apache.bcel.internal.generic.RETURN;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class JedisUtils {

    //1.定义一个连接池对象
    private final static JedisPool POOL;

    static {
        //初始化
        //1.设置连接池的配置对象
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(50);
        config.setMaxTotal(15);

        POOL = new JedisPool(config, "192.168.122.3", 6379);
    }

    /**
     * 从连接池中获取连接
     * @return
     */
    public static Jedis getJedis() {
        Jedis jedis = POOL.getResource();
        jedis.auth("123456");
        return jedis;
    }

}
