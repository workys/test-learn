package com.ys.string;

public class StringBuilderDemo {
    public static void main(String[] args) throws InterruptedException {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int j = 0; j < 1000; j++) {
                        sb.append("a");
                    }
                }
            }).start();
        }
        Thread.sleep(100);
        System.out.println(sb.length());
        System.out.println(sb);
    }
}
