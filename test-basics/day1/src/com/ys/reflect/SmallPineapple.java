package com.ys.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class SmallPineapple {
    public String name;
    public int age;
    public double weight;//体重只有自己知道


    public SmallPineapple() {
    }

    public SmallPineapple(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void getInfo() {
        System.out.println("[" + name + " 的年龄是：" + age + "]");
    }

    public static void main(String[] args) throws Exception {
        Class class1 = Class.forName("com.ys.reflect.SmallPineapple");
        Class class2 = SmallPineapple.class;
        Class class3 = new SmallPineapple().getClass();

        System.out.println("Class.forName() == SmallPineapple.class:" + (class1 == class2));
        System.out.println("Class.forName() == isntance.getClass():" + (class1 == class3));
        System.out.println("instance.getClass() == SmallPineapple.class:" + (class2 == class3));

        SmallPineapple smallPineapple = (SmallPineapple) class1.newInstance();
        smallPineapple.getInfo();

        Constructor constructor = class1.getConstructor(String.class, int.class);
        constructor.setAccessible(true);
        SmallPineapple smallPineapple11 =
                (SmallPineapple) constructor.newInstance("小萝卜", 21);
        smallPineapple11.getInfo();

        Field[] fields1 = class1.getFields();
        Field[] fields2 = class1.getDeclaredFields();

        Set<Field> allFields = new HashSet<>();
        allFields.addAll(Arrays.asList(fields1));
        allFields.addAll(Arrays.asList(fields2));
        System.out.println(allFields);
    }
}
