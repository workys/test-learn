package com.test.ys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Java8Learn1 {

    public static void main(String[] args) {
        List<String> names1 = new ArrayList<>();
        names1.add("google");
        names1.add("taobao");
        names1.add("sina");
        names1.add("amazon");
        names1.add("facebook");

        List<String> names2 = new ArrayList<>();
        names2.add("google");
        names2.add("taobao");
        names2.add("sina");
        names2.add("amazon");
        names2.add("facebook");

        Java8Learn1 tester = new Java8Learn1();
        System.out.println("使用 Java 7 语法: ");

        tester.sortUsingJava7(names1);
        System.out.println(names1);
        System.out.println("使用 Java 8 语法: ");

        tester.sortUsingJava8(names2);
        System.out.println(names2);

    }

    private void sortUsingJava7(List<String> names){
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });
    }

    private void sortUsingJava8(List<String> names){
        Collections.sort(names, (s1, s2) -> s1.compareTo(s2));
    }
}
