package com.ys.service;


import java.io.Serializable;

public class ServiceResultTO<T> implements Serializable {

    private static final long serialVersionUID = 123456789l;

    private  Boolean success;

    private String message;

    private T data;

}
