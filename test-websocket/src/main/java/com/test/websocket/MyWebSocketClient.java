package com.test.websocket;

import jdk.nashorn.internal.ir.WhileNode;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.enums.ReadyState;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.Iterator;

public class MyWebSocketClient extends WebSocketClient {


    public MyWebSocketClient(URI serverUri) {
        super(serverUri);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
        System.out.println("------ MyWebSocket onOpen -------");
        for (Iterator<String> it = handshakedata.iterateHttpFields(); it.hasNext(); ) {
            String key = it.next();
            System.out.println(key + ":" + handshakedata.getFieldValue(key));
        }
    }

    @Override
    public void onMessage(String message) {
        System.out.println("------  接收到服务端参数： " + message + "-------");
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        System.out.println("------  MyWebSocket onClose -------");
    }

    @Override
    public void onError(Exception e) {
        System.out.println("------  MyWebSocket onError -------");
    }


    public static void main(String[] args) throws Exception {
        MyWebSocketClient myClient = new MyWebSocketClient(new URI("ws://localhost:8080/test_websocket_war_exploded/myWebSocket"));
        //连接
        myClient.connect();

        while(!myClient.getReadyState().equals(ReadyState.OPEN)){
            System.out.println("MyWebSocketClient 连接中 ...");
        }

        // 往websocket服务端发送数据
        myClient.send("MyWebSocketClient Message");

        //调用此方法保持住连接
        myClient.sendPing();

        Thread.sleep(3000);
        myClient.close();
    }
}
