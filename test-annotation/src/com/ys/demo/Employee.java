package com.ys.demo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Table("ys_employee")
public class Employee {
     @Column(value = "employee_id",property = "varchar(30)")
     private  String employeeid;
     @Column(value = "dept_id",property = "varchar(30)")
     private  String deptid;
     @Column(value = "employee_name",property = "varchar(30)")
     private  String employeename;
     @Column(value = "sex",property = "int(2)")
     private int sex;
     @Column(value = "brithday",property = "varchar(30)")
     private String brithday;
     @Column(value = "married",property = "int(2)")
     private int married;
     @Column(value = "salary",property = "varchar(30)")
     private int salary;
     @Column(value = "degree",property = "varchar(30)")
     private String degree;
     @Column(value = "email",property = "varchar(30)")
     private String email;
}
