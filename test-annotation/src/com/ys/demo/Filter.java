package com.ys.demo;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
@Table("user")
public class Filter {

    @Column(value = "id",property = "int(30)")
    private int id;

    @Column(value = "user_name",property = "varchar(30)")
    private String userName;

    @Column(value = "nick_name",property = "varchar(30)")
    private String nickName;

    @Column(value = "age",property = "varchar(30)")
    private int age;

    @Column(value = "city",property = "varchar(30)")
    private String city;

    @Column(value = "email",property = "varchar(30)")
    private String email;

    @Column(value = "mobile",property = "varchar(30)")
    private String mobile;
}
