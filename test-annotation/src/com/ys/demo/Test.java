package com.ys.demo;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Test {
    /**
     * 根据对象、获取sql
     *
     * @param f
     * @return
     */
    private static String querySql(Filter f) {
        StringBuilder sb = new StringBuilder();
        //1、获取到class
        Class c = f.getClass();
        //2.获取到table的名字
        boolean iExists = c.isAnnotationPresent(Table.class);
        if (!iExists) {
            return null;
        }
        Table t = (Table) c.getAnnotation(Table.class);
        String tableName = t.value();
        sb.append("select * from ");
        sb.append(tableName);
        sb.append(" where 1 = 1 ");
        //3、遍历所有的字段
        Field[] fArray = c.getDeclaredFields();
        //4、处理每个字段对应的sql
        for (Field field : fArray) {
            boolean iFExists = field.isAnnotationPresent(Column.class);
            if (!iFExists) {
                continue;
            }
            //4.1、拿到字段的名字
            Column column = field.getAnnotation(Column.class);
            String columnName = column.value();

            //4.2 根据反射方法名，拿到字段的值
            String fieldName = field.getName();
            String getMethodName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
            Object fieldValue = null;
            try {
                Method method = c.getMethod(getMethodName);
                fieldValue = method.invoke(f);
            } catch (Exception e) {
                e.printStackTrace();
            }

            //4.3拼装sql
            if (fieldValue == null || (fieldValue instanceof Integer && (Integer) fieldValue == 0)) {
                continue;
            }
            sb.append(" and ").append(columnName).append(" = ");
            if(fieldValue instanceof  String){
                if (((String) fieldValue).contains(",")){
                    String[] values = ((String) fieldValue).split(",");
                    sb.append(" in(");
                    for (String value : values) {
                        sb.append("'").append(value).append("'").append(',');
                    }
                    sb.deleteCharAt(sb.length()-1);
                    sb.append(")");
                }else{
                    sb.append("'").append(fieldValue).append("'");
                }
            }else{
                sb.append(fieldValue);
            }
        }
        return sb.toString();
    }

    private static String createTable(Object f) {
        StringBuilder sb = new StringBuilder();
        Class  c = f.getClass();
        //2.获取到table的名字
        boolean iExists = c.isAnnotationPresent(Table.class);
        if (!iExists) {
            return null;
        }
        Table t = (Table) c.getAnnotation(Table.class);
        String tableName = t.value();
        sb.append("create table " + tableName + "( ");
        //3、遍历所有的字段
        Field[] fArray = c.getDeclaredFields();
        for (Field field : fArray) {
            boolean iFExists = field.isAnnotationPresent(Column.class);
            if (!iFExists) {
                continue;
            }
            //4.1、拿到字段的名字
            Column column = field.getAnnotation(Column.class);
            String columnName = column.value();
            String property = column.property();
            sb.append( columnName  + " " + property + ","+"\r\n");
        }
        //sb.deleteCharAt(sb.length()-3);
        sb.append(" PRIMARY KEY (id) ");
        sb.append(" )ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        return sb.toString();
    }

    private static String insertData(Object o){
        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        sb1.append(" VALUES (");  
        Class c = o.getClass();

        //2.获取到table的名字
        boolean iExists = c.isAnnotationPresent(Table.class);
        if (!iExists) {
            return null;
        }
        Table table = (Table) c.getAnnotation(Table.class);
        String tablename = table.value();
        sb.append("insert into "+ tablename + " (");

        Field[] fields = c.getDeclaredFields();
        for (Field field : fields) {
            boolean iFExists = field.isAnnotationPresent(Column.class);
            if (!iFExists) {
                return null;
            }
            Column column = field.getAnnotation(Column.class);
            String filedname = column.value();
            sb.append(filedname + ",");

            //插入值
            String fieldName = field.getName();
            String  modeName =  "get" + fieldName.substring(0,1).toUpperCase() +  fieldName.substring(1);
            Object fieldValue = null;
            try {
                Method method = c.getMethod(modeName);
                fieldValue = method.invoke(o);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (fieldValue instanceof String){
               sb1.append(" '" +fieldValue +"'" +",");
            } else if (fieldValue instanceof Integer) {
                sb1.append(" "+fieldValue+",");
            }  else if (fieldValue == null){
                sb1.append(" " + null +",");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(")");
        sb.append(" " + sb1.deleteCharAt(sb1.length() - 1));
        sb.append(")");
        return sb.toString();
    }




    public static void main(String[] args) {
       /* Filter f1 = new Filter();
        f1.setId(10);

        Filter f2 = new Filter();
        f2.setCity("辽东");

        Filter f3 = new Filter();
        f3.setMobile("123304,213213,343");

        String sql1 = querySql(f1);
        String sql2 = querySql(f2);
        String sql3 = querySql(f3);



        System.out.println(sql1);
        System.out.println(sql2);
        System.out.println(sql3);*/
        for (int i = 0; i <100 ; i++) {
            Employee el = new Employee();

            String sql1 = createTable(el);
            String insertsql = insertData(el);
            System.out.println(sql1);
            System.out.println(insertsql);

        }

    }
}
