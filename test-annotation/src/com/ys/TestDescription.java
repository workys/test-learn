package com.ys;

@Description(desc = "I am class annotation", author = "ys", age = 26)
public class TestDescription {
    @Description(desc = "I am method annotation", author = "ys", age = 19)
    public String test() {
        return "red";
    }

}
