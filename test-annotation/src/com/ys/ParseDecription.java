package com.ys;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class ParseDecription {


    public static void main(String[] args) {
        //1.使用类加载器加载类
        try {
            Class c = Class.forName("com.ys.TestDescription");
            System.out.println(c);

            //2.找到类上面的注释
            boolean isExist = c.isAnnotationPresent(Description.class);

            if (isExist) {
                //3、拿到注解实例
                Description d = (Description) c.getAnnotation(Description.class);
                System.out.println("========parse class annotation========");
                System.out.println("desc = " + d.desc());
                System.out.println("author = " + d.author());
                System.out.println("age = " + d.age());
            }

            //4、找到方法上的注解
            Method[] ms = c.getMethods();
            for (Method m : ms) {
                boolean isMExit = m.isAnnotationPresent(Description.class);
                if (isMExit) {
                    Description d = m.getAnnotation(Description.class);
                    System.out.println("========parse method annotation========");
                    System.out.println("desc = " + d.desc());
                    System.out.println("author = " + d.author());
                    System.out.println("age = " + d.age());
                }
            }

            //另外一种解析方法
            for (Method m : ms) {
                Annotation[] as = m.getAnnotations();
                for (Annotation a : as) {
                    if (a instanceof Description) {
                        Description d = (Description) a;
                        System.out.println("=======parse method annotation other way=======");
                        System.out.println("desc = " + d.desc());
                        System.out.println("author = " + d.author());
                        System.out.println("author = " + d.age());
                    }

                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
